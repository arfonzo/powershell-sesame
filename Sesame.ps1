﻿# SESAME FOR POWERSHELL
#
#   The Sesame Name Resolver Service queries several databases from the name of an
#   astronomical object (outside the Solar System bodies), and displays some fundamental
#   parameters (type of object, J2000 position). The databases queried are Simbad, NED
#   and VizieR.
#
#   AUTHOR: arfonzo@gmail.com

param (
    #[Parameter(Mandatory=$true)]
    [string]$Query,
    [switch]$About,
    [string]$Databases,
    [switch]$Debug,
    [switch]$Help,
    [switch]$IgnoreCache,
    [switch]$MultipleResolutions,    
    [switch]$OutputAllIdentifiers,
    [switch]$OutputFluxes,
    [switch]$ProperMotions,
    [switch]$Xml
)

function WriteLogo {
    Write-Host "";
    Write-Host "`t" -NoNewline;
    Write-Host "                       " -BackgroundColor DarkGreen;
    Write-Host "`t" -NoNewline;
    Write-Host " SESAME FOR POWERSHELL " -ForegroundColor Green -BackgroundColor DarkGreen;
    Write-Host "`t" -NoNewline;
    Write-Host "                       " -BackgroundColor DarkGreen;
    Write-Host "`t  " -NoNewline;
    Write-Host "";
}

if ($About) {
    WriteLogo;
    Write-Host "";
    Write-Host "arfonzo@gmail.com";
    Write-Host "";
    return;
}

if ($Help) {
    WriteLogo;
    Write-Host "The Sesame Name Resolver Service queries several databases from the name of an astronomical object (outside the Solar System bodies), and displays some fundamental parameters (type of object, J2000 position). The databases queried are Simbad, NED, and VizieR.";
    Write-Host "";
    Write-Host "Paramaters:" -ForegroundColor Green;
    Write-Host "  -Query <String>`t`tItem to search. Required."
    Write-Host "`t`t`t`tEx: -Query `"CYG X-1`"" -ForegroundColor DarkCyan
    Write-Host "  -About`t`t`tShow information about this script and exit."
    Write-Host "  -Databases <String>`t`tPossible choices of databases - [S]imbad, [N]ED, [V]izieR, [A]ll (default)."
    Write-Host "`t`t`t`tEx - Simbad and NED: -Databases SN" -ForegroundColor DarkCyan
    Write-Host "  -Help`t`t`t`tShow help and exit."
    Write-Host "  -IgnoreCache`t`t`tIgnore cached results."
    Write-Host "  -MultipleResolutions`t`tReturn all results, if more than one."
    Write-Host "  -OutputAllIdentifiers`t`tOutput all Identifiers for objects retrieved. Default: main designation only."
    Write-Host "  -OutputFluxes`t`t`tOutput fluxes (magnitudes) for objects retrieves (Simbad only)."
    Write-Host "  -Xml`t`t`t`tReturn results as XML and exit."
    Write-Host "";
    return;
}

if (!$Query) {
    Write-Error "The -Query parameter cannot be empty, my dear fool. Use -Help to get an overview.";
    return;
}

# By default query all sources.
$sesopts = "SNV";

if ($Databases) {
    # Overwrite if asked.
    $sesopts = $Databases;
}

if ($MultipleResolutions) {
    # Return more than the first result.
    $sesopts += "A";
}

if ($NoCache) {
    $sesopts = "`~" + $sesopts;
}

$opts = "-oxp";

if ($OutputAllIdentifiers) {
    $opts += "I";
} elseif ($OutputFluxes) {
    $opts += "F";
}

$sesame = "http://cdsweb.u-strasbg.fr/cgi-bin/nph-sesame/-oxpI/" + $sesopts + "?";
$query = $query -replace ' ','+';

Write-Progress -Activity "Querying sesame and friends..." -Status "0% Complete:" -PercentComplete 0;

$sesameXml = New-Object System.Xml.XmlDocument;
$sesameXml.Load($sesame + $query);

if ($Xml) {
    return $sesameXml;
}

# Build table of types, from http://vizier.u-strasbg.fr/viz-bin/OType
$otypes = @{};
# template
$otypes.Add("XYZ", "DESC");
# Unknown
$otypes.Add("?", "Object of unknown nature");
$otypes.Add("ev", "unknown transient event");
# Radio
$otypes.Add("Rad", "Radio-source");
$otypes.Add("mR", "metric Radio-source");
$otypes.Add("cm", "centimetric Radio-source");
$otypes.Add("mm", "millimetric Radio-source");
$otypes.Add("smm", "sub-millimetric Radio-source");
$otypes.Add("HI", "HI (21cm) source");
$otypes.Add("rB", "radio Burst");
$otypes.Add("Mas", "Maser");
# IR
$otypes.Add("IR", "Infra-Red source");
$otypes.Add("FIR", "Far-IR source (λ >= 30 µm)");
$otypes.Add("NIR", "Near-IR source (λ <= 10 µm)");
# Red
$otypes.Add("red", "very red source");
$otypes.Add("ERO", "Extremely Red Object");


# Galaxy
$otypes.Add("AGN", "Active Galaxy Nucleus");
# Misc
$otypes.Add("HXB", "High Mass X-ray Binary");
$otypes.Add("PN", "Planetary Nebula");

WriteLogo;
Write-Host "";
Write-Host " Results for: $($sesameXml.Sesame.Target.name) " -BackgroundColor DarkCyan -ForegroundColor White;
Write-Host "";

# Counter for progress bar
$counter = 0;
$counterMax = $sesameXml.Sesame.Target.Resolver.Count;
if ($counterMax -lt 1) { $counterMax = 1 };

$sesameXml.Sesame.Target.Resolver | ForEach-Object {

    # Display progress bar.
    [int]$I = $counter/$counterMax * 100;
    Write-Progress -Activity "Querying sesame and friends..." -Status "$I% Complete:" -PercentComplete $I;
    $counter++;

    if ($_.name -eq $null) {
        Write-Host "No results found.";
        return;
    }

    # Skip Resolutions with no results.
    if ($_.INFO -notcontains "Zero (0) answers") {
        $name = $_.name.split(' ')[0].split('=')[1];
        # Print Resolver name.
        Write-Host " $name " -BackgroundColor DarkBlue -ForegroundColor White -NoNewline;
        # Latency
        Write-Host " $($_."#comment".split('[')[0].TrimEnd().split(' ')[1])" -ForegroundColor DarkGray;

        # Display information line, if available.
        if ($_.INFO) {
            Write-Host "`tInfo:`t$($_.INFO)" -ForegroundColor DarkGreen;
        }

        if ($_.oname) {
            Write-Host "`tName:`t$($_.oname)" -ForegroundColor Magenta;
        }

        if ($_.alias) {
            $t = "";
            $_.alias | ForEach-Object {
                $t += $_ + ', ';
            }
            $t = $t.Substring(0,$t.Length -2);

            #$t = $_.alias;
            #$t = $_.alias -replace '\n','| ';
            Write-Host "`tAlias:`t$($t)" -ForegroundColor Gray;
        }

        if ($_.otype) {
            Write-Host "`tType:`t$($_.otype)" -ForegroundColor White -NoNewline;
            if ($otypes.$($_.otype) -ne $null) {
                Write-Host " ($($otypes.$($_.otype)))" -ForegroundColor Gray;
            } else {
                Write-Host "";
            }

        }

        if ($_.jpos) {
            Write-Host "`tJPos:`t$($_.jpos)" -ForegroundColor Gray;
        }

        if ($_.jradeg) {
            Write-Host "`tJRads:`t$($_.jradeg)" -ForegroundColor Gray;
        }

        if ($_.jdedeg) {
            Write-Host "`tJDegs:`t$($_.jdedeg)" -ForegroundColor Gray;
        }

        if ($_.refpos) {
            Write-Host "`tRefPos:`t$($_.refpos)" -ForegroundColor Gray;
        }

        # Link to further info
        if ($name -eq "Simbad") {
            Write-Host "`tHTTP:`thttp://simbad.u-strasbg.fr/simbad/sim-id?Ident=$query" -ForegroundColor Gray;
        }

        # pm
        # http://simbad.u-strasbg.fr/simbad/sim-display?data=meas#pm
        if ($ProperMotions -and $_.pm) {
            Write-Host "";
            Write-Host "Measurements: Stellar Proper Motions" -ForegroundColor White -BackgroundColor DarkCyan;
            $_.pm;
        }

        # Extra info
        if ($Debug) {
            Write-Host "DEBUG CRAP:" -ForegroundColor DarkRed;
            $_;
        }

        Write-Host "";
    }
    
}
