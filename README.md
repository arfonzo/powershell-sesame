# Sesame for PowerShell #

This thing is currently a work in progress. Use at your own risk.

### What is this? ###

The Sesame Name Resolver Service queries several databases from the name of an
astronomical object (outside the Solar System bodies), and displays some fundamental parameters (type of object, J2000 position).

The databases queried are Simbad, NED and VizieR.